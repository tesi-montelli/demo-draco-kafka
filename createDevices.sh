#!/bin/bash
NUM_THERM=$1
NUM_CAMERA=$2
NUM_ROBOTS=$3

echo "num robots create devices"
echo ${NUM_ROBOTS}

#dare il launch, per questa demo si può partire da ON
for ((i=1; i<=${NUM_THERM}; i++))
do
  echo "therm ${i}"
  devices/thermometerMQTT/launcher.sh ${i} off 1000 0 $((${i}%${NUM_ROBOTS}))  &>/dev/null &
done


for ((i=1; i<=${NUM_CAMERA}; i++))
do
  echo "camera ${i}"
  devices/cameraMQTT/launcher.sh ${i} off 1000 $((${i}%${NUM_ROBOTS})) &>/dev/null &
done
