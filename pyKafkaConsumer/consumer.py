import json
from kafka import KafkaConsumer

consumer = KafkaConsumer(
    'test',
     bootstrap_servers=['137.204.74.59:7777'],
     auto_offset_reset='earliest',
     enable_auto_commit=True,
     group_id='my-group',
     value_deserializer=lambda x: json.loads(x.decode('utf-8')))

ids = []
for message in consumer:
    id = message.value["id"]
    if not id in ids:
        ids.append(id)
        print(ids)