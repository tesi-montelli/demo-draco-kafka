#!/bin/bash
NUM_THERM=$1
NUM_CAMERA=$2
NUM_ROBOTS=$3

./setupFiware.sh

./setupKafka.sh

./createDevices.sh ${NUM_THERM} ${NUM_CAMERA} ${NUM_ROBOTS}
