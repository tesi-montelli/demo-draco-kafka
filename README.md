# Setup and configuration

## Build devices
```bash
docker build -t fiware-device/thermometer ./devices/thermometerMQTT
docker build -t fiware-device/camera ./devices/cameraMQTT
```

## fiware
The first time the demo is run it is necessary to manually start fiware in order to verify the correct functioning and give the possibility to download the necessary files; for subsequent starts it is sufficient to use the `launch.sh` script as described below.

```bash
docker-compose up --build
```

Upon setup completion all the docker containers must be shut off

### webserver
edit `webserver/html/index.html` if you need to access the dashboard from another machine

```javascript
Vue.mixin({
  data: () => ({
    fiwareIP: 'fiware-orion',
    webServerIP: PLACE MACHINE IP HERE,
    IOTAPort: '4041',
    OCBPOrt: '8082',
    webServerPort: '8080'
  }),
})
```

## kafka
The first time the demo is run it is necessary to manually start kafka in order to verify the correct functioning and give the possibility to download the necessary files; for subsequent starts it is sufficient to use the `launch.sh` script as described below.
Edit `kafka-docker/.env` and replace the default value for `IP` with the ip address of the machine that will run the docker container

```bash
docker-compose --file kafka-docker/docker-compose.yaml up --build
```

Upon setup completion all the docker containers must be shut off

# Launch Experiment

```bash
./launch 5 2 2
```

1. number of thermometers
2. number of cameras
3. number of robots

## Load NiFi/Draco flow
See the attached presentation for a graphical guide

1. access NiFi dashboard at `127.0.0.1:9090`
2. click the "upload template" button in the "operate" menu in the left part of the screen
3. upload `draco/flow/demo.xml`
4. load the uploaded template by dragging the "Template" button on the main portion of the screen
  - select the uploaded template and confirm with ok
5. configure the "PublishKafkaRecord" block
  - in the "properties" tab:
    - in "Kafka Broker":
      1.  set ip and port on which the kafka broker runs (default: port 7777 and local machine's IP)
    - in "Record Reader":
      1. click on the arrow on the right-most part of the arrow, a new window will popup
      2. for both rows, click on the enable button in the right-most part of the table
      3. close the popup
6. press ctrl+a to select all the blocks and press the play button in the "operate" menu

# Interact with the demo

## Kafka consumer
A very simple python script is made available to verify the correct reception of messages via kafka.
The script adds the device IDs contained in the received messages to a list.
At the beginning all the devices are turned off, the following section shows how they can be started in order to generate a sequence of messages.

It is assumed that python3 is used.

### required libraries
- kafka-python
  - can be installed via pip (`pip install python-kafka`)

## Access dashboard
A dashboard will be available at `127.0.0.1:8080`.
The dashboard shows a card for each device, you can turn on or off devices by interacting with the dedicated buttons placed on each card.

## Access saved record

1. run an interactive instance of `mongo` image
  - `docker run -it --network demo-draco-kafka_default  --entrypoint /bin/bash mongo`
2. connect to the running `mongo-db-orion-instance`
  - `mongo --host mongo-db-orion`
3. use the correct DB
  - `use sth_entities`
4. check which collection exists
  - `show collections`
    - a collection has been created for each type of device
5. mongo commands can be used to interact with the collection
  - eg: `db["sth_Thermometer"].find().limit(1)`

## Stop the demo
execute `stop.sh`

# Note: problems with windows

When executing this demo on windows you may encounter some problems regarding port binding, to resolve run the following commands in Powershell (execute as administrator)
```
net stop hns
net start hns
```
